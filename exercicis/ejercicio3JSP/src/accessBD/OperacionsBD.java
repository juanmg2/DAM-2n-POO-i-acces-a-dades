package accessBD;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import beans.Film;
import beans.Language;

public class OperacionsBD {
	protected Connection connexio = null;
	protected Statement st = null;
	protected ResultSet rs = null;
	
	protected Connection getConnection() throws SQLException, ClassNotFoundException {
		if (connexio == null) {
			Class.forName("com.mysql.jdbc.Driver");
			connexio = DriverManager.getConnection("jdbc:mysql://localhost/sakila",
						"root","");
		}
		return connexio;
	}
	
	protected void closeConnection() {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				;
			} finally {
				rs = null;
			}
		}
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			} finally {
				st = null;
			}
		}
		if (connexio != null) {
			try {
				connexio.close();
			} catch (SQLException e) {
				;
			} finally {
				connexio = null;
			}
		}
	}
	
	public List<Film> consultaPellicules() throws SQLException, ClassNotFoundException {
		List<Film> pellicules = new ArrayList<Film>();
		try {
			connexio = getConnection();
			st = connexio.createStatement();
			String sql = "select film.*, language.name from film film join language language on (film.language_id = language.language_id);";
			rs = st.executeQuery(sql);
		
			while (rs.next()) {
				Film f = new Film(rs.getInt("film.film_id"), rs.getString("film.title"),
						rs.getString("film.description"), rs.getString("film.release_year"),
						rs.getInt("length"),rs.getString("language.name"),rs.getInt("film.original_language_id"));
				pellicules.add(f);
			}
		} finally {
			closeConnection();
		}
		return pellicules;
	}
	
	
	
	public void insereixPellicula(Film f) throws SQLException, ClassNotFoundException {
		try {
			connexio = getConnection();
			st = connexio.createStatement();
			String sql = "insert into film(film_id, title, description," +
					"release_year, language_id, original_language_id," +
					"length) values('"
					+f.getFilmId()+"','"
					+f.getTitle()+"','"
					+f.getDescription()+"','"
					+f.getReleaseYear()+"','"
					+f.getLanguageId()+"','"
					+f.getOriginalLanguageId()+"','"
					+f.getLength()+"');";
			st.executeUpdate(sql);
		} finally {
			closeConnection();
		}
	}
	
	
}
