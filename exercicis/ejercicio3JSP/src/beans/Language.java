package beans;

public class Language {
	private int languageId;
	private String languageName;
	public Language(int languageId, String languageName) {
		super();
		this.languageId = languageId;
		this.languageName = languageName;
	}
	public int getLanguageId() {
		return languageId;
	}
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	
	
}
