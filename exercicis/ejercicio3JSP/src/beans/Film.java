package beans;

public class Film {
	private int filmId;
	private String title;
	private String description;
	private String releaseYear;
	private String idiomas;
	private Integer languageId;
	private Integer originalLanguageId;
	private int length;
	public int getFilmId() {
		return filmId;
	}
	public Film() {
		;
	}
	public Film(int filmId, String title, String description,
			String releaseYear,
			int length, String idiomas, int originalLanguageId) {
		super();
		this.filmId = filmId;
		this.title = title;
		this.description = description;
		this.releaseYear = releaseYear;
		this.length = length;
		this.idiomas = idiomas;
		this.originalLanguageId = originalLanguageId;
	}
	
	public String getIdiomas() {
		return idiomas;
	}
	public void setIdiomas(String idiomas) {
		this.idiomas = idiomas;
	}
	public void setFilmId(int filmId) {
		this.filmId = filmId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getReleaseYear() {
		return releaseYear;
	}
	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}
	public Integer getLanguageId() {
		return languageId;
	}
	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}
	public Integer getOriginalLanguageId() {
		return originalLanguageId;
	}
	public void setOriginalLanguageId(Integer originalLanguageId) {
		this.originalLanguageId = originalLanguageId;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	@Override
	public String toString() {
		return "Film [filmId=" + filmId + ", title=" + title + ", description="
				+ description + ", releaseYear=" + releaseYear
				+ ", languageId=" + languageId + ", originalLanguageId="
				+ originalLanguageId + ", length=" + length + "]";
	}
	
}
