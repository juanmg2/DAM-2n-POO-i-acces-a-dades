import java.util.Random;

public abstract class Animal implements Esser{
	static Random random = new Random();
	public abstract String mou(Zoo zoo);
	public abstract String alimenta(Zoo zoo);
	public abstract String expressa(Zoo zoo);
	
	public String accio(Zoo zoo){
		String cadena="";
		int num;
		num = random.nextInt(3);
		
		if(num==0){
			cadena=mou(zoo);
		}
		if(num==1){
			cadena=alimenta(zoo);
		}
		if(num==2){
			cadena=expressa(zoo);
		}
		return cadena;
	}

}
