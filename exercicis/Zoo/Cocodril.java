
public class Cocodril extends Animal {

	@Override
	public String mou(Zoo zoo) {
		String cadena="Un cocodril es mou";
		return cadena;
	}

	@Override
	public String alimenta(Zoo zoo) {
		String cadena="";
		Animal animal = zoo.mostraAnimal();
		if(animal instanceof Vaca){
			zoo.suprimeixAnimal(animal);
			cadena="El cocodril es menja una vaca";
		}else if(animal instanceof Cocodril ){
			zoo.suprimeixAnimal(animal);
			cadena="El cocodril es menja un altre cocodril";
		}else if(animal==null && animal.equals(this)){
			cadena="Un cocodril busca a qui es pot menjar";
		}
		return cadena;
	}

	@Override
	public String expressa(Zoo zoo) {
		String cadena="El cocodril obre la boca plena de dents";
		return cadena;
	}

}
