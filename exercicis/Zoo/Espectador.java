
public class Espectador implements Esser{

	@Override
	public String accio(Zoo zoo) {
		String cadena="";
		Animal animal = zoo.mostraAnimal();
		if(animal instanceof Vaca){
			cadena ="Un espectador mira una vaca";
			
		}else if(animal instanceof Cocodril){
			cadena="Un espectador mira un cocodril";
		}else if(animal == null){
			cadena ="Un espectador no sap que fer";
		}
		return cadena;
	}
	
}
