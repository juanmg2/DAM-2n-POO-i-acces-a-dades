import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class Zoo {
	private ArrayList<Animal> animales=new ArrayList<Animal>();/*definicion de arrayList*/
	private Espectador espectador = new Espectador();
	 Random random = new Random();
	
	public Animal mostraAnimal(){
		Animal animal;
		int posicion = random.nextInt(animales.size());/*obtenemos una posicion aleatoria de la lista*/
		
		if(posicion>=0){
			animal = animales.get(posicion);/*obtenemos un animal de la lista*/
			return animal;
		}else{
			return null;
		}
	}
	public void afegeixAnimal(String cadena){
		if(cadena.equals("vaca")){
			animales.add(new Vaca());
		}else if(cadena.equals("cocodril")){
			animales.add(new Cocodril());
		}
	}
	public void afegeixAnimal(Animal animal){
		if(animal instanceof Vaca){
			animales.add(new Vaca());
		}
		if(animal instanceof Cocodril){
			animales.add(new Cocodril());
		}
	}
	
	public void suprimeixAnimal(String cadena){
		int i=0;
		if(cadena.equals("vaca")){
			for(Animal animal :animales){
				i++;
				if(animal instanceof Vaca && i==1){
					animales.remove(animal);
				}
			}
			
		}else if(cadena.equals("cocodril")){
			for(Animal animal :animales){
				i++;
				if(animal instanceof Cocodril && i==1){
					animales.remove(animal);
				}
			}
		}
		
	}
	public void suprimeixAnimal(Animal animal){
		animales.remove(animal);
	}
	public void suprimeixTots(String cadena){
		if(cadena.equals("vaca")){
			for(Animal animal :animales){
				if(animal instanceof Vaca){
					animales.remove(animal);
				}
			}
			
		}else if(cadena.equals("cocodril")){
			for(Animal animal :animales){
				if(animal instanceof Cocodril){
					animales.remove(animal);
				}
			}
		}
	}
	public void mira(){
		int num=random.nextInt(2);
		int posicion=random.nextInt(animales.size());
		Zoo zoo = null;
		if(num==0){
			espectador.accio(zoo);
		}else if(num==1){
			for(Animal animal :animales){
				if(animales.size()>0){
					animal = animales.get(posicion);
					animal.accio(zoo);
				}else{
					System.out.println("Quin zoo m�s aborrit");
				}
			}
		}
	}
	
	
}
