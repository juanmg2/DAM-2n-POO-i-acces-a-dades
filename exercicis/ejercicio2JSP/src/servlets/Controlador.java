package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Film;

import accessBD.OperacionsBD;

/**
 * Servlet implementation class Controlador
 */
public class Controlador extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String op = req.getParameter("accio");
		try {
			if (op.equals("inserir")) {
				Film f = (Film) req.getAttribute("film");
				OperacionsBD bd = new OperacionsBD();
				bd.insereixPellicula(f);
				resp.sendRedirect("index.html");
			}
		} catch (SQLException e) {
			req.setAttribute("errorSQL", e.getMessage());
 			resp.sendError(resp.SC_BAD_REQUEST, "errorSQL");
			
		} catch (ClassNotFoundException e) {
			throw new IOException(e);
		}
	}

	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String op = request.getParameter("accio");
		
		try {
			if (op.equals("llista")) {
				OperacionsBD bd = new OperacionsBD();
				List<Film> pellicules = bd.consultaPellicules();
				request.setAttribute("films", pellicules);
				RequestDispatcher rd = request.getRequestDispatcher("/llista.jsp");
				rd.forward(request, response);
			}
			if (op.equals("alta")) {
				response.sendRedirect("alta.jsp");
			}
			
		} catch (SQLException e) {
			throw new IOException(e);
		} catch (ClassNotFoundException e) {
			throw new IOException(e);
		}
	}

}
