package ej1;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FicherosLove {

	public static void main(String[] args) {
		if (args.length == 1) {
            Path dir = Paths.get(args[0]);
            System.out.println("Fitxers del directori " + dir);
            if (Files.isDirectory(dir)) {
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
                    for (Path fitxer: stream) {
                    	String permiso="";
                    	if(Files.isReadable(fitxer)){
                    		permiso+="R";
                    	}else{
                    		permiso+="-";
                    	}
                    	if(Files.isWritable(fitxer)){
                    		permiso+="W";
                    	}else{
                    		permiso+="-";
                    	}
                    	if(Files.isExecutable(fitxer)){
                    		permiso+="E";
                    	}else{
                    		permiso+="-";
                    	}
                       
                    	System.out.println(permiso+" "+fitxer);
                       
                    }
                } catch (IOException | DirectoryIteratorException ex) {         
                    System.err.println(ex);
                }
            } else {
                System.err.println(dir.toString()+" no �s un directori");
            }
        } else {
            System.err.println("�s: java LlistarDirectori <directori>");
        }
    }
	}

	
	




