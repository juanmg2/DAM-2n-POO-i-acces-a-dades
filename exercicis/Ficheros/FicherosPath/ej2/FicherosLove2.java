package ej2;

import java.awt.List;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Deque;
import java.util.LinkedList;

/*Fes un programa que rebi un nom de directori com a par�metre i mostri el seu contingut, indicant en cada cas si es tracta d'un fitxer o directori i els permisos que tenim sobre ell.
El programa actuar� recursivament, mostrant el contingut de tots els subdirectoris amb qu� es vagi trobant.
Per fer-ho, utilitza una pila on es guardin els noms de directoris pendents de mostrar.*/

public class FicherosLove2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Deque<Path> pila = new LinkedList<Path>();
		if (args.length == 1) {
            Path dir = Paths.get(args[0]);
            System.out.println("Fitxers del directori " + dir);
            
            if (Files.isDirectory(dir)) {
            	 pila.push(dir);
            	 while(pila.size()>0){
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(pila.pop())) {
                    for (Path fitxer: stream) {
                    	if (Files.isDirectory(fitxer))
                    	   pila.push(fitxer);
                    	String permiso="";
                    	if(Files.isReadable(fitxer)){
                    		permiso+="R";
                    	}else{
                    		permiso+="-";
                    	}
                    	if(Files.isWritable(fitxer)){
                    		permiso+="W";
                    	}else{
                    		permiso+="-";
                    	}
                    	if(Files.isExecutable(fitxer)){
                    		permiso+="E";
                    	}else{
                    		permiso+="-";
                    	}
                       
                    	System.out.println(permiso+" "+fitxer.getFileName());
              
                    }
                }catch (IOException | DirectoryIteratorException ex) {         
                    System.err.println(ex);
                }
            }	 
                 
            } else {
                System.err.println(dir.toString()+" no �s un directori");
            }
        } else {
            System.err.println("�s: java LlistarDirectori <directori>");
        }

	}
}


