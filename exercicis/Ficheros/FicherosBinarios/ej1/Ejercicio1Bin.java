package ej1;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class Ejercicio1Bin {

	public static void main(String[] args) {
		int codigo=0;
		String secreto="";
		Random random = new Random();
		char[] caracter ="abcdefghijklmn�opqrstuvwyz".toCharArray();/*transforma una cadena en un array de caracteres*/
		
		try (DataOutputStream clave = new DataOutputStream(new FileOutputStream("dades.bin"))) {
			for( int i =0; i<1000; i++){
				codigo+= random.nextInt(3)+1;
				clave.writeInt(codigo);	
				for(int j=0; j<3;j++){
					secreto+=caracter[ random.nextInt(caracter.length)];/*la cadena secreto estara compuesta por 
																		3 caracteres, que se escogen aleatoriamente de la longitud del array de chars
																		caracter.*/
				}
				clave.writeChars(secreto);
			}
		} catch (FileNotFoundException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }

	}

}
