package ej2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Ejercicio2Bin {

	public static void main(String[] args) {
		for(String ficheros :args){
			int contador=0;
		try (DataInputStream fichero = new DataInputStream(new FileInputStream(ficheros))) {
			 System.out.println("Nombre del fichero "+ficheros);
			while(contador!=-1){
				contador= fichero.read();
				if(contador!=-1){
					if(Character.isAlphabetic(contador)){
						System.out.println((char)contador);
					}
				}
			}
					
		} catch (FileNotFoundException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }

	}
	}
	}


