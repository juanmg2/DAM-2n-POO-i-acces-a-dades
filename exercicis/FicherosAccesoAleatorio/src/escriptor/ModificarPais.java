package escriptor;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

import cercarSecret.Pais;


public class ModificarPais {

	
		// TODO Auto-generated method stub
		private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
			StringBuilder b = new StringBuilder();
			char ch = ' ';
			for (int i = 0; i < nChars; i++) {
				ch = fitxer.readChar();
				if (ch != '\0')
					b.append(ch);
			}
			return b.toString();
		}

		public static Scanner scanner = new Scanner(System.in);

		public static void main(String[] args) {
			int id;
			String opcio;
			System.out.println("Introdueix el camp a registrar: ");
			id = scanner.nextInt();
			scanner.nextLine();
			System.out.println();
			mostrarPais(id);
			System.out.println();
			System.out.println("Introdueix que vols modificar (Nom, Codi, Poblacio, Capital): ");
			opcio = scanner.next();
			scanner.nextLine();
			opcio.toLowerCase();
			switch (opcio) {
			case "nom":
				System.out.println("Modificar Nom:");
				modificarNom(id);
				break;
			case "codi":
				System.out.println("Modificar Codi:");
				modificarCodi(id);
				break;
			case "poblacio":
				System.out.println("Modificar Poblaci�:");
				modificarPoblacio(id);
				break;
			case "capital":
				System.out.println("Modificar Capital:");
				modificarCapital(id);
				break;
			default:
				System.err.println("Opcio erroni");
				break;
			}
			scanner.close();
		}

		public static void mostrarPais(int id) {
			Pais p;
			String nom, capital, codiISO;
			int poblacio;
			long pos = 0;

			try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "r")) {
				pos = (id - 1) * 174;
				if (pos < 0 || pos >= fitxer.length())
					throw new IOException("N�mero de registre inv�lid.");
				fitxer.seek(pos);
				System.out.println("Pa�s: " + fitxer.readInt());
				nom = readChars(fitxer, 40);
				codiISO = readChars(fitxer, 3);
				capital = readChars(fitxer, 40);
				poblacio = fitxer.readInt();
				p = new Pais(nom, codiISO, capital);
				p.setPoblacio(poblacio);
				System.out.println(p);

			} catch (IOException e) {
				System.err.println(e);
			}
		}

		public static void modificarNom(int id) {
			String nouNom;
			long pos = 0;
			StringBuilder b = null;
			try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
				pos = (id - 1) * 174;
				if (pos < 0 || pos >= fitxer.length())
					throw new IOException("N�mero de registre inv�lid.");
				System.out.println("Introdueix el nou nom: ");
				nouNom = scanner.next();
				scanner.nextLine();
				if (nouNom.length() > 0 && !nouNom.equals("")) {
					fitxer.seek(pos + 4);
					b = new StringBuilder(nouNom);
					b.setLength(40);
					fitxer.writeChars(b.toString());
				} else {
					System.err.println("Nom inv�lid");
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}

		public static void modificarCodi(int id) {
			String nouCodi;
			long pos = 0;
			try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
				pos = (id - 1) * 174;
				if (pos < 0 || pos >= fitxer.length())
					throw new IOException("N�mero de registre inv�lid.");
				System.out.println("Introdueix el nou codi: ");
				nouCodi = scanner.next();
				scanner.nextLine();
				if (nouCodi.length() > 0 && nouCodi.length() <= 3 && !nouCodi.equals("")) {
					fitxer.seek(pos + 84);
					fitxer.writeChars(nouCodi);
				} else {
					System.err.println("Codi inv�lid");
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}

		public static void modificarCapital(int id) {
			String novaCapital;
			long pos = 0;
			StringBuilder b = null;
			try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
				pos = (id - 1) * 174;
				if (pos < 0 || pos >= fitxer.length())
					throw new IOException("N�mero de registre inv�lid.");
				System.out.println("Introdueix la nova capital: ");
				novaCapital = scanner.next();
				scanner.nextLine();
				if (novaCapital.length() > 0 && !novaCapital.equals("")) {
					fitxer.seek(pos + 90);
					b = new StringBuilder(novaCapital);
					b.setLength(40);
					fitxer.writeChars(b.toString());
				} else {
					System.err.println("Capital inv�lida");
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}

		public static void modificarPoblacio(int id) {
			int novaPoblacio;
			long pos = 0;
			try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
				pos = (id - 1) * 174;
				if (pos < 0 || pos >= fitxer.length())
					throw new IOException("N�mero de registre inv�lid.");
				System.out.println("Introdueix la nova poblaci�: ");
				novaPoblacio = scanner.nextInt();
				scanner.nextLine();
				if (novaPoblacio > 0) {
					fitxer.seek(pos + 170);
					fitxer.writeInt(novaPoblacio);
				} else {
					System.err.println("La poblaci� ha de ser positiva");
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}


