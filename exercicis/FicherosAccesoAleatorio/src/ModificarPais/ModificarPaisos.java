package ModificarPais;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

import cercarSecret.Pais;

public class ModificarPaisos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
StringBuilder valor = null;
 		
 		Scanner sc = new Scanner(System.in);
 		long numRegistre = 0;
 		int dada;
 
 		Pais paisTemp = null;

         
        try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
        	
        	System.out.print("Quin registre vols canviar? ");
        	numRegistre = sc.nextInt();
        	
            if (numRegistre < 1 || numRegistre > fitxer.length() / 174)
            	throw new IOException("N�mero de registre inv�lid.");    
            

            paisTemp = llegirRegistre(numRegistre);
            

            System.out.println(paisTemp.toString());
                    
            do {
            	System.out.print("Quina dada vols canviar? [1 nom | 2 codi | 3 capital | 4 poblacio] ");
            	dada = sc.nextInt();
            	sc.nextLine();
            }while(dada < 1 || dada > 4);
            
            System.out.print("Introdueix el nou valor: ");
            
            switch (dada) {
            	case 1:
            		valor = new StringBuilder(sc.nextLine());
            		valor.setLength(40);
            		paisTemp.setNom(valor.toString());
            		break;
            	case 2:
            		valor = new StringBuilder(sc.nextLine());
            		valor.setLength(3);
            		paisTemp.setCodiISO(valor.toString());
            		break;
            	case 3:
            		valor = new StringBuilder(sc.nextLine());
            		valor.setLength(40);
            		paisTemp.setCapital(valor.toString());
            		break;
            	case 4:
            		paisTemp.setPoblacio(sc.nextInt());
            		break;
            }
            
            escriureRegistre(paisTemp, numRegistre);
            
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
 
        sc.close();
	}
	

	private static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
		
        StringBuilder lectura = new StringBuilder();
        
        char ch = ' ';
        for (int i = 0; i < nChars; i++) {
            ch = fitxer.readChar();
            if (ch != '\0')
                lectura.append(ch);
        }
        return lectura.toString();
    }
	

	private static Pais llegirRegistre(long numRegistre) {
		
		Pais pais = null;
        String nom, capital, codiISO;
        int poblacio;
        long posicio;
        
        try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "r")) {
        	
        	posicio = (numRegistre - 1) * 174;
            fitxer.seek(posicio);       
            fitxer.readInt();							
            nom = readChars(fitxer, 40);
            codiISO = readChars(fitxer, 3);
            capital = readChars(fitxer, 40);
            poblacio = fitxer.readInt();
            
            pais = new Pais(nom, codiISO, capital);
            pais.setPoblacio(poblacio);
            
        } catch (IOException e) {
            System.err.println(e);
        }
        
        return pais;
	}

	private static void escriureRegistre(Pais nouPais, long numRegistre) {
		
		long posicio;
		StringBuilder cadenaTemp;
		
		try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
						
			posicio = (numRegistre-1) * 174;

			fitxer.seek(posicio + 4);

			cadenaTemp = new StringBuilder(nouPais.getNom());
			cadenaTemp.setLength(40);
			fitxer.writeChars(cadenaTemp.toString());

			cadenaTemp = new StringBuilder(nouPais.getCodiISO());
			cadenaTemp.setLength(3);
			fitxer.writeChars(cadenaTemp.toString());

			cadenaTemp = new StringBuilder(nouPais.getCapital());
			cadenaTemp.setLength(40);
			fitxer.writeChars(cadenaTemp.toString());

			fitxer.writeInt(nouPais.getPoblacio());
		}
		catch (IOException e) {
			System.err.println(e);
		}
	}

}
