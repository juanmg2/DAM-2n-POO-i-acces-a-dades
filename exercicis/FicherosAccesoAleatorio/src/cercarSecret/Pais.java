package cercarSecret;

public class Pais {
	private String nom;
	private String codiISO;
	private int poblacio;
	private String capital;

	public Pais(String nom, String codiISO, String capital) {
		if (codiISO.length() != 3)
			throw new IllegalArgumentException("Els codis ISO sempre tenen 3 car�cters.");
		this.nom = nom;
		this.codiISO = codiISO;
		this.capital = capital;
	}

	public int getPoblacio() {
		return poblacio;
	}

	public void setPoblacio(int poblacio) {
		if (poblacio < 0)
			throw new IllegalArgumentException("La poblaci� ha de ser positiva.");
		this.poblacio = poblacio;
	}
	
	public void setNom(String nom) {
		if (nom.equals(""))
			throw new IllegalArgumentException("Has de posar un nom");
		this.nom = nom;
	}

	public void setCodiISO(String codiISO) {
		this.codiISO = codiISO;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public String getNom() {
		return nom;
	}

	public String getCodiISO() {
		return codiISO;
	}

	public String getCapital() {
		return capital;
	}

	@Override
	public String toString() {
		return "Pa�s " + nom + " (" + codiISO + "), capital: " + capital + ", poblaci�: " + poblacio;
	}
}
