package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Film;
import beans.ActorFilm;

import accessBD.OperacionsBD;

/**
 * Servlet implementation class Controlador
 */
public class Controlador extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controlador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String op = request.getParameter("accio");
		
		try {
			
			if (op.equals("llistaPelicules")) {
				OperacionsBD bd = new OperacionsBD();
				List<Film> pellicules = bd.consultaPellicules();
				request.setAttribute("films", pellicules);
				RequestDispatcher rd = request.getRequestDispatcher("/llista.jsp");
				rd.forward(request, response);
			}		
			if (op.equals("llistaActors")){
				OperacionsBD bd = new OperacionsBD();
				List<ActorFilm> actors = bd.consultaActors();
				request.setAttribute("actorsFilms", actors);
				RequestDispatcher rd = request.getRequestDispatcher("/llistaActors.jsp");
				rd.forward(request, response);
			}
		} catch (SQLException e) {
			throw new IOException(e);
		} catch (ClassNotFoundException e) {
			throw new IOException(e);
		}
		
		
		
	}

}
