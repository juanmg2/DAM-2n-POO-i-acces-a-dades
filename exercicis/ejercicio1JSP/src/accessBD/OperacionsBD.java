package accessBD;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import beans.Film;
import beans.ActorFilm;

public class OperacionsBD {
	protected Connection connexio = null;
	protected Statement st = null;
	protected ResultSet rs = null;
	
	protected Connection getConnection() throws SQLException, ClassNotFoundException {
		if (connexio == null) {
			Class.forName("com.mysql.jdbc.Driver");
			connexio = DriverManager.getConnection("jdbc:mysql://localhost/sakila",
						"root","");
		}
		return connexio;
	}
	
	protected void closeConnection() {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				;
			} finally {
				rs = null;
			}
		}
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				;
			} finally {
				st = null;
			}
		}
		if (connexio != null) {
			try {
				connexio.close();
			} catch (SQLException e) {
				;
			} finally {
				connexio = null;
			}
		}
	}
	
	public List<Film> consultaPellicules() throws SQLException, ClassNotFoundException {
		List<Film> pellicules = new ArrayList<Film>();
		try {
			connexio = getConnection();
			st = connexio.createStatement();
			String sql = "select * from film;";
			rs = st.executeQuery(sql);
		
			while (rs.next()) {
				Film f = new Film(rs.getInt("film_id"), rs.getString("title"),
						rs.getString("description"), rs.getString("release_year"),
						rs.getInt("language_id"), rs.getInt("original_language_id"),
						rs.getInt("length"));
				pellicules.add(f);
			}
		} finally {
			closeConnection();
		}
		return pellicules;
	}
	
	
	
	public List<ActorFilm> consultaActors() throws SQLException, ClassNotFoundException {
		List<ActorFilm> actor = new ArrayList<ActorFilm>();
		try {
			connexio = getConnection();
			st = connexio.createStatement();
			String sql = "select actor.*, film.title from actor actor join film_actor film_actor on (actor.actor_id = film_actor.actor_id)"
					+ "join film film on (film_actor.film_id = film.film_id);";
			rs = st.executeQuery(sql);
		
			while (rs.next()) {
				ActorFilm a = new ActorFilm(rs.getInt("actor.actor_id"), rs.getString("actor.first_name"),
						rs.getString("actor.last_name"), rs.getString("film.title"));
				actor.add(a);
			}
		} finally {
			closeConnection();
		}
		return actor;
	}
	

}
