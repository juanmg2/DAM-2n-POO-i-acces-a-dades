<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="beans.ActorFilm,java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Actors</title>
</head>
<body>
	<h1>Llista d'Actors</h1>
<table border='1'>
<tr><th>Codi</th><th>Nom</th><th>Cognom</th><th>T�tol pel�l�cula</th></tr>
<%
@SuppressWarnings("unchecked")
List<ActorFilm> actorsFilms = (List<ActorFilm>) request.getAttribute("actorsFilms");
for (ActorFilm a : actorsFilms) {%>
	<tr><td><%=a.getActorId() %></td>
	<td><%=a.getFirstName() %></td>
	<td><%=a.getLastName() %></td>
	<td><%=a.getFilmName() %></td>
	
<%}%>
</table>
<a href="index.html">Inici</a>
</body>
</html>