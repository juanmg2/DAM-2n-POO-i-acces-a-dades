<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="beans.Film,java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Llista de pel·lícules</title>

</head>

<body>
<h1>Llista de pel·lícules</h1>
<table border='1'>
<tr><th>Codi</th><th>Nom</th><th>Descripció</th><th>Any</th><th>Idioma</th>
<th>Idioma original</th><th>Durada</th></tr>
<%
@SuppressWarnings("unchecked")
List<Film> pellicules = (List<Film>) request.getAttribute("films");
for (Film f : pellicules) {%>

		<tr><td><%=f.getFilmId() %></td>
		<td><%=f.getTitle() %></td>
		<td><%=f.getDescription() %></td>
		<td><%=f.getReleaseYear() %></td>
		<td><%=f.getLanguageId() %></td>
		<td><%=f.getOriginalLanguageId() %></td>
		<td><%=f.getLength() %></td>
<%}%>
</table>
<a href="index.html">Inici</a>
</body>
</html>