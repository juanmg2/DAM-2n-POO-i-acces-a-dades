import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Properties;

import com.mysql.jdbc.Statement;

public class Ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				String url = "jdbc:mysql://localhost:3306/sakila";
		        Properties propietatsConnexio = new Properties();
		        propietatsConnexio.setProperty("user", "root");
		    
		        try (Connection con = DriverManager.getConnection(url,propietatsConnexio);
		        		Statement st = (Statement) con.createStatement();) {
		            System.out.println("Base de dades connectada!");
		            try (ResultSet rs = st.executeQuery("select store.store_id, address.address, staff.first_name, staff.last_name "+
		            									"from  address address join  store store on(address.address_id = store.address_id "+
		            									"join  staff staff on (store.store_id = staff.store_id)")) {
		                System.out.println("Fes un programa que mostri totes les botigues que hi ha, amb les seves adreces i el nom i cognom del seu encarregat. :");
		                
		                System.out.println("\n\nId Tienda   Direccion  Nombre    Apellido\n");
		                while (rs.next()) {
		                	int store = rs.getInt("store.store_id");
		                	String direccion  = rs.getString("address.address");
		                	String nombre = rs.getString("staff.first_name");
		                	String apellido = rs.getString("staff.last_name");
		                	System.out.println(""+store+"   "+direccion+"   "+nombre+"    "+apellido);
		                	
		                	
		                	
		                }
		            }
		        } catch (SQLException e) {
		            System.err.println("Error SQL: " + e.getMessage());
		        }
	}

}
