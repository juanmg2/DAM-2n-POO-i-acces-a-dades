import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Properties;

import com.mysql.jdbc.Statement;

public class Ejercicio2 {

	public static void main(String[] args) {
		
		String url = "jdbc:mysql://localhost:3306/sakila";
        Properties propietatsConnexio = new Properties();
        propietatsConnexio.setProperty("user", "root");
    
        try (Connection con = DriverManager.getConnection(url,propietatsConnexio);
        		Statement st = (Statement) con.createStatement();) {
            System.out.println("Base de dades connectada!");
            try (ResultSet rs = st.executeQuery("select actor.first_name, actor.last_name from actor join film_actor on (actor.actor_id=film_actor.actor_id) join film on (film.film_id=film_actor.film_id) where film.title = 'TWISTED PIRATES'")) {
                System.out.println("nom i cognom dels actors que apareixen a la pel�l�cula TWISTED PIRATES :");
                while (rs.next()) {
                	String nombreActor = rs.getString("actor.first_name");
                	String apellidoActor = rs.getString("actor.last_name");
                	
                	
                	System.out.println("Nombre: "+nombreActor+" Apellido: "+apellidoActor);
               
                	
                	
                }
            }
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }
	}

}
