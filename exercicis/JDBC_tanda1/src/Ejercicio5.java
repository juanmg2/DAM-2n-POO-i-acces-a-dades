import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.mysql.jdbc.Statement;

public class Ejercicio5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost:3306/sakila";
        Properties propietatsConnexio = new Properties();
        propietatsConnexio.setProperty("user", "root");
    
        try (Connection con = DriverManager.getConnection(url,propietatsConnexio);
        		Statement st = (Statement) con.createStatement();) {
            System.out.println("Base de dades connectada!");
            try (ResultSet rs = st.executeQuery("SELECT customer.first_name, customer.last_name, address.phone, film.title"
           	+ " FROM customer customer JOIN address address ON (customer.address_id = address.address_id)"
           	+ " JOIN rental rental ON (customer.customer_id = rental.customer_id)"
           	+ " JOIN inventory inventory ON (rental.inventory_id = inventory.inventory_id)"
           	+ " JOIN store store ON (inventory.store_id = store.store_id)"
           	+ " JOIN film film ON (inventory.film_id = film.film_id)"
           	+ " WHERE rental.return_date is null AND DATE_ADD(rental.rental_date, INTERVAL film.rental_duration DAY) < CURDATE()"
           	+ " ORDER BY customer.first_name")) {
                System.out.println("Fes un programa que localitzi els clients que tenen pel�l�cules que ja haurien d'haver retornat. Volem el nom i cognom i el tel�fon dels clients i el t�tol de la pel�l�cula que deuen. :");
                
                System.out.println("\n\n\n");
                while (rs.next()) {
                	
                	String nombre  = rs.getString("customer.first_name");
                	String apellido= rs.getString("customer.last_name");
                	String pelicula = rs.getString("film.title");
                	String telefono = rs.getString("address.phone");
                	System.out.println(""+nombre+"   "+apellido+"   "+telefono+"    "+pelicula);
                	
                	
                	
                }
            }
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }

	}

}
