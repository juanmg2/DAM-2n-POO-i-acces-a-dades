import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.mysql.jdbc.Statement;

public class Ejercicio6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost:3306/sakila";
        Properties propietatsConnexio = new Properties();
        propietatsConnexio.setProperty("user", "root");
    
        try (Connection con = DriverManager.getConnection(url,propietatsConnexio);
        		Statement st = (Statement) con.createStatement();) {
            System.out.println("Base de dades connectada!");
            try (ResultSet rs = st.executeQuery("select category.name, COUNT(film.film_id) AS 'Numero Peliculas' "+
            									"from category category join  film_category film_category on(category.category_id = film_category.category_id) "+
            									"join film film on (film.film_id = film_category.category_id) "+
            									"group by category.name")) {
                System.out.println("Fes un programa que mostri les diferents categories de pel�l�cules que tenim i quantes pel�l�cules de cada categoria hi ha.. :");
                
                System.out.println("\n\nCategoria    Numero Peliculas \n");
                while (rs.next()) {
                	int numero = rs.getInt("Numero Peliculas");
                	String categoria = rs.getString("category.name");
                	
                	System.out.println(""+categoria+"      "+numero+"");
                	
                	
                	
                }
            }
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }
	}

}
