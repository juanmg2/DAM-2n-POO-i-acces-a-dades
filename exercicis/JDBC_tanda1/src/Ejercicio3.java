import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Properties;

import com.mysql.jdbc.Statement;

public class Ejercicio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost:3306/sakila";
        Properties propietatsConnexio = new Properties();
        propietatsConnexio.setProperty("user", "root");
    
        try (Connection con = DriverManager.getConnection(url,propietatsConnexio);
        		Statement st = (Statement) con.createStatement();) {
            System.out.println("Base de dades connectada!");
            try (ResultSet rs = st.executeQuery("select film.title, store.store_id, date_add(rental.rental_date ,INTERVAL film.rental_duration DAY)  AS actual_rental_date, customer.first_name "+
            			 "from inventory inventory join store store on (inventory.store_id = store.store_id) "+
                         "join film film on (inventory.film_id= film.film_id) "+
                         "join rental rental on (inventory.inventory_id= rental.inventory_id) "+
						 "join customer customer on (rental.customer_id = customer.customer_id) "+                       
						 "where customer.first_name like 'ALLISON' AND customer.last_name like 'STANLEY'")) {
                System.out.println("Fes un programa que mostri quins �tems i de quines botigues t� en lloguer el client de nom ALLISON STANLEY. Tamb� haur� de mostrar la data de retorn d'aquests �tems.:");
                
                System.out.println("\n\nTitulo Pelicula   Id Tienda    Fecha de devolucion    Nombre cliente\n");
                while (rs.next()) {
                	String titFilm = rs.getString("film.title");
                	int idTenda = rs.getInt("store.store_id");
                	LocalDate fechaRental = rs.getDate("actual_rental_date").toLocalDate();
                	String customer = rs.getString("customer.first_name");
                	System.out.println(""+titFilm+"   "+idTenda+"   "+fechaRental+"    "+customer);
                	
                	
                	
                }
            }
        } catch (SQLException e) {
            System.err.println("Error SQL: " + e.getMessage());
        }
	}

}
