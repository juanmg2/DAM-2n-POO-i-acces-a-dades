import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Properties;

import com.mysql.jdbc.Statement;

public class Ejercicio1 {

	public static void main(String[] args) {
		 String url = "jdbc:mysql://localhost:3306/sakila";
	        Properties propietatsConnexio = new Properties();
	        propietatsConnexio.setProperty("user", "root");
	    
	        try (Connection con = DriverManager.getConnection(url,propietatsConnexio);
	                Statement st = (Statement) con.createStatement();) {
	            System.out.println("Base de dades connectada!");
	            try (ResultSet rs = st.executeQuery("select title, description, length from film where length > 150")) {
	                System.out.println("Dades de la taula film:");
	                while (rs.next()) {
	                	String title = rs.getString("title");
	                	String description = rs.getString("description");
	                	String length = rs.getString("length");
	                	
	                	System.out.println("Titulo: "+title);
	                	System.out.println("Descripcion: "+description);
	                	System.out.println("Duracion: "+length+"\n");
	                	
	                	
	                }
	            }
	        } catch (SQLException e) {
	            System.err.println("Error SQL: " + e.getMessage());
	        }
	    }

	}

