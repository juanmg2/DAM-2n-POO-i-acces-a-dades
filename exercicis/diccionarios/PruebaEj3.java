package ej3;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class PruebaEj3 {
	
	public static void main(String[] args) {
		
		Map<String, Integer> compra = new HashMap<String, Integer>();
		Set<String> claus = compra.keySet();
		/*a) Crea un diccionari amb aquestes dades.*/
	
		compra.put("tom�quets", 6);
		compra.put("flams", 4);
		compra.put("pizzes", 2);
		compra.put("llaunes de tonyina", 2);
		compra.put("blat de moro", 5);
		compra.put("enciam", 1);
		
		/*b) A partir del diccionari, imprimeix nom�s la llista d'ingredients.*/
		System.out.println("Productes:");
		for (String clau : claus){
		    System.out.println(clau);
		}
		/*c) Imprimeix la quantitat de tom�quets.*/
		System.out.println("\nla quantitat de tom�quets: "+compra.get("tom�quets"));
	
	/*d) Imprimeix aquells ingredients dels quals n'h�gim de comprar m�s de 3 unitats.*/
		System.out.println("\nProductes de m�s de 3 unitats");
		for (String clau : claus){
			if(compra.get(clau) >3)
		    System.out.println(clau);
		}
		
	/*e) Pregunta a l'usuari un ingredient i retorna el n�mero d'unitats que s'han de comprar. Si l'ingredient no �s a la llista de 
	 * la compra, retorna un missatge d'error.*/
		Scanner sc = new Scanner(System.in);
		System.out.print("\nintrodueix un produce per saber si es a la llista: ");
		String opcion= sc.nextLine();
		
	if(compra.containsKey(opcion)){
		System.out.println("S'han de comprar: "+compra.get(opcion)+ " "+opcion);
	}else{
		 throw new RuntimeException("Aquest ingredient no es troba a la llista");
	}
		
		
	/*f) Imprimeix tota la llista amb aquest format:
			ingredient (unitats a comprar)*/
		System.out.println("\nLlista de la compra:");
		for (String clau : claus){
		    System.out.println(clau+"("+compra.get(clau)+")");
		}
	
		/*g) A partir del diccionari, sumar totes les quantitats i donar el n�mero total d'�tems que hem de comprar. Resultat: 20.*/
		int suma=0;
		for(String clau:claus){
			suma+=compra.get(clau);
		}
		System.out.println("productes totals de la llista "+suma);
		
		/*h) Pregunta a l'usuari un ingredient, despr�s demana-li un nou valor d'unitats i 
		 modifica l'entrada corresponent al diccionari.*/
		System.out.println("Producte que vols modificar el seu contingut: ");
		String opcion2= sc.nextLine();
		if(compra.containsKey(opcion2)){
			System.out.println("Introdueix uan quantitat: ");
			int quantitat = sc.nextInt();
			compra.replace(opcion2, quantitat);
			System.out.println("El canvi es de : "+opcion2+" "+compra.get(opcion2));
		}else{
			 throw new RuntimeException("Aquest ingredient no es troba a la llista");
		}
		
		
}
}
