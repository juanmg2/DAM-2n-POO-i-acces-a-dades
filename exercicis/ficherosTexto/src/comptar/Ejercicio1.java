package comptar;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Introdueix ruta del fitxer: ");
		String nombreArchivo = sc.nextLine();
        char ch; 
        int cant = 0;
        int lectura;
      
        
        try {
 
        	FileReader lector = new FileReader(nombreArchivo);

            while ((lectura = lector.read())!=-1) {

                ch = (char) lectura;
 
                if (ch == 'a')
                	cant++;
            }    
            System.out.println("El documento" + nombreArchivo + " tiene " + cant + " letras a.");
            sc.close();
            lector.close();       
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
                    
	}
	}


