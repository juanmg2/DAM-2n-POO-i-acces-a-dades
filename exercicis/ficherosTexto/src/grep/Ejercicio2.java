package grep;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Deque;
import java.util.LinkedList;

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Deque<Path> archivos = new LinkedList<Path>();
		String palabra;
		

		if (args.length >= 2) {
			
			palabra = args[0];
			

			for (int i = 1; i < args.length; i++)			
				archivos.push(Paths.get(args[i]));			

        	while (archivos.size() > 0) {

        		Path path = archivos.pop();

            	if (Files.isDirectory(path)) {
            		
        			try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
	                    for (Path element: stream)
	                    	archivos.push(element);
	                    
                    } catch (IOException | DirectoryIteratorException ex) {   
                        System.err.println(ex);
                    }
            	}
            	else {
            		leerArchivo(path, palabra);
            	}            	                        	   
        	}
		}
	}
				
	public static void leerArchivo(Path archivo, String paraula) {
		

		String linia;

		int numLinia = 0;
		try (BufferedReader lector = new BufferedReader(new FileReader(archivo.toString()))) {
			
			while ((linia = lector.readLine()) != null) {
				if (linia.contains(paraula)) {
					System.out.println(linia + "\n" + archivo + "\t" + numLinia);					
				}		
				numLinia++;
			}
			lector.close();
		}
		catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	}


