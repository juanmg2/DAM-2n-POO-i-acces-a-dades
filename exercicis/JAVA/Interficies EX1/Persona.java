package ExircisisInterfaces;

import java.util.Comparator;

public class Persona implements Comparable <Persona> {
	private double peso;
	private int edad;
	private int altura;


	public Persona(double peso, int edad, int altura) {
		this.peso = peso;
		this.edad = edad;
		this.altura = altura;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}


	public int getAltura() {
		return altura;
	}
	public void setAltura(int altura) {
		this.altura = altura;
	}

	public int compareTo(Persona p) {
		return 0;

    }


	public static final Comparator<Persona> COMPARA_PESO = new Comparator<Persona> () {
		@Override
		public int compare(Persona p1, Persona p2) {
	        if (p1.getPeso()<p2.getPeso()) {
	        	System.out.println("La persona p1 es mas delgada, que la persona p2");
	        	return  1;
	        } else if (p1.getPeso()>p2.getPeso()) {
	        	System.out.println("La persona p1 es mas gorda, que la persona p2");
	        	return  -1;
	        	}
	        System.out.println("La persona p1 pesa lo mismo que la persona p2");
	        return 0;

		}
	};

	public static final Comparator<Persona> COMPARA_ALTURA = new Comparator<Persona> () {
		@Override
		public int compare(Persona p1, Persona p2) {

	        if (p1.getAltura()<p2.getAltura()) {
	        	System.out.println("La persona p1 es mas bajita, que la persona p2");
	        	return  1;
	        } else if (p1.getAltura()>p2.getAltura()) {
	        	System.out.println("La persona p1 es mas alta, que la persona p2");
	        	return  -1;
	        	}
	        System.out.println("La persona p1 mide lo mismo que la persona p2");
	        return 0;

		}


	};


	public static final Comparator<Persona> COMPARA_EDAD = new Comparator<Persona> () {
		@Override
		public int compare(Persona p1, Persona p2) {

	        if (p1.getEdad()<p2.getEdad()) {
	        	System.out.println("La persona p1 es menor, que la persona p2");
	        	return  -1;

	        } else if (p1.getEdad()>p2.getEdad()) {
	        	System.out.println("La persona p1 es mayor, que la persona p2");
	        	return  1;

	        	}
	        else{
	        	System.out.println("La persona p1 tiene la misma edad que la persona p2");
	        	return 0;

		}
};
};
}
