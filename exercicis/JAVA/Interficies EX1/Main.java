package ExircisisInterfaces;

public class Main {

	public static void main(String[] args) {
		Persona p1 = new Persona(86.65, 25,170);
		Persona p2 = new Persona(90.65, 30,180);


		Persona.COMPARA_EDAD.compare(p1, p2);
		Persona.COMPARA_ALTURA.compare(p1, p2);
		Persona.COMPARA_PESO.compare(p1, p2);

	}
}
