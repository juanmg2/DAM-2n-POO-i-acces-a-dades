package ej2;

public class NegEntreDos implements Serie{
	
	private double numAct;
	
	public NegEntreDos(){
		numInicial();
	}
	
	public NegEntreDos(double num){
		numDefecto(num);
	}
	
	@Override
	public void numInicial() {
		numAct= 2;
		
	}

	@Override
	public void numDefecto(double num) {
		numAct = num;
		
	}

	@Override
	public double numSiguiente() {
		numAct = numAct /-2;
		return numAct;
	}

}
