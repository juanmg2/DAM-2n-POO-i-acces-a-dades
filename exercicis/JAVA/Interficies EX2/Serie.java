package ej2;

public interface Serie {
	public void numInicial();
	public void numDefecto(double num);
	public double numSiguiente();
}
