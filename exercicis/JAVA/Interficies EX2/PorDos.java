package ej2;

public class PorDos implements Serie{
	
	private double numAct;
	
	public PorDos() {
		numInicial();
		
	}
	
	public PorDos(double num){
		numDefecto(num);
	}

	
	public void numInicial() {
		numAct =2;
		
	}

	
	public void numDefecto(double num) {
		numAct = num;
		
	}

	@Override
	public double numSiguiente() {
		numAct = numAct *2;
		return numAct;
		
	}

}
