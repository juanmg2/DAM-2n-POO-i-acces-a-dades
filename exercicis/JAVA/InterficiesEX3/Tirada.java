package ej3;

import java.util.Arrays;
import java.util.Random;

public class Tirada implements Comparable <Tirada>,Cloneable{
	private Random random = new Random();
	private int ataque;
	private int defensa;
	private int numDados;
	private Integer[] dados;
	
	public Tirada(int ataque, int defensa, int numDados){
		this.ataque=ataque;
		this.defensa= defensa;
		this.numDados=numDados;
		this.dados= new Integer[numDados];
		this.generarTirada();
		
	}
	
	public int getAtaque() {
		return ataque;
	}
	
	public void setAtaque(int ataque) {
		this.ataque = ataque;
	}

	public int getDefensa() {
		return defensa;
	}
	public void setDefensa(int defensa) {
		this.defensa = defensa;
	}

	public int getNumDados() {
		return numDados;
	}

	public void setNumDados(int numDados) {
		this.numDados = numDados;
	}

	public Integer[] getDados() {
		return dados;
	}


	public void setDados(Integer[] Dados) {
		this.dados = dados;
	}

	public void generarTirada(){
		for(int i =0;i<numDados;i++){
			dados[i]=random.nextInt(6)+1;
		}
		Arrays.sort(dados);
	}
	@Override
	public Tirada clone(){	
		try{
			return (Tirada) super.clone();
		}catch (CloneNotSupportedException e){
			return null;
		}		
	}

	@Override
	public int compareTo(Tirada t) {
		int resultado =0;
		for(int i=0;i<numDados;i++){
			resultado+= ((ataque + dados[i]) > (t.getDefensa() + 
							(i >= t.getDados().length ? 0 : t.getDados()[i])) ? 1 : 0);
		}
		
		return resultado;
	}
	
	@Override
	public String toString(){
		return String.format("ATAQUE: %d DEFENSA: %d  DADOS (%d): %s ",ataque, defensa,numDados,Arrays.toString(dados) );
	}

}
