package ej3;

public class PruebaTirada {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Tirada tirada1 = new Tirada(3,1,5);
		Tirada tirada2= new Tirada(2,2,4);
		
		System.out.println(tirada1.toString());
		System.out.println(tirada2.toString());
		
		System.out.println("Comparacion 2 tiradas diferentes: "+(tirada1.compareTo(tirada2)-tirada2.compareTo(tirada1)));
		
		Tirada tirada3 =  tirada1.clone();
		
		System.out.println("Comparacion 2 tiradas iguales:" +(tirada1.compareTo(tirada3)-tirada3.compareTo(tirada1)));
	}

}
