package ejercicio3;
/*Donada la seg�ent llista de popularitat dels llenguatges, on el primer �s el m�s popular i l'�ltim el que menys: llenguatges = "Java",     "C", "C++", "Visual Basic", "PHP",     "Python", "Perl", "C#",     "JavaScript","Delphi"
Extreu (no cal que facis un men�, simplement que el programa extregui seq�encialment):
El llenguatge m�s popular de la llista.
El sis� m�s popular.
Els tres m�s populars.
Els tres menys populars.
Tots menys el primer.
Imprimeix la llista amb cada llenguatge en una l�nia i numerada.
Nombre total de llenguatges a la llista.*/

import java.util.ArrayList;
import java.util.List;

public class Prueba3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List <String> lenguajes = new ArrayList<String>();
		lenguajes.add("Java");
		lenguajes.add("C");
		lenguajes.add("C++");
		lenguajes.add("Visual Basic");
		lenguajes.add("PHP");
		lenguajes.add("Python");
		lenguajes.add("Perl");
		lenguajes.add("C#");
		lenguajes.add("JavaScript");
		lenguajes.add("Delphi");
		System.out.println("El llenguatge m�s popular: "+lenguajes.get(0));
		System.out.println("El sis� m�s popular: "+lenguajes.get(5)+"\n");
		
		System.out.println("\nEls 3 mes populars son");
		for(int i =0; i<3;i++){
			System.out.println("El numero " +i+ " es: " +lenguajes.get(i));
		}
		
		System.out.println("\nEls 3 menys populars son");
		for(int i =lenguajes.size()-1;i>lenguajes.size()-4;i--){
			System.out.println("El numero " +i+ " es: " +lenguajes.get(i));
		}
		
		System.out.println("\nTots menys el primer");
		for(int i =1;i<lenguajes.size();i++){
			System.out.println("El numero " +i+ " es: " +lenguajes.get(i));
		}
		
		System.out.println("\nLlista amb una linea numerada");
		int num=0;
		for(String lenguaje : lenguajes ){
			num++;
			System.out.println("- "+num+ " "+lenguaje);
		}
		int num2=0;
		for(String lenguaje: lenguajes){
			num2++;
		}
		System.out.println("\nNumero de lenguajes:"+num2);
	}

}
