package mongodb.examen;

import java.util.Random;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExExamen3b {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("test");
		MongoCollection<Document> coll = db.getCollection("images");
		
		Random random = new Random();
		
		coll.find().forEach((Document doc) -> {
			coll.updateOne(doc, new Document("$set", new Document("likes", random.nextInt(101))));
		});
		
		client.close();
	}

}
