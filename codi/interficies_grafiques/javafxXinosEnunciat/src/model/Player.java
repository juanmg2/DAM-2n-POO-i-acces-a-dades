package model;

import java.util.Random;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Player {
	private static Random random = new Random();
	/*
	 * TODO: crea dos SimpleIntegerProperty per inicialitzar aquests atributs.
	 */
	private IntegerProperty hiddenCoins;
	private IntegerProperty bet;
	
	public int getHiddenCoins() {
		return hiddenCoins.get();
	}
	
	public IntegerProperty getHiddenCoinsProperty() {
		return hiddenCoins;
	}
	
	public void setHiddenCoins(int hiddenCoins) {
		if (hiddenCoins < 0 || hiddenCoins > 3)
			throw new IllegalArgumentException("Quantitat de monedes invàlida");
		this.hiddenCoins.set(hiddenCoins);
	}
	
	public void hide() {
		hiddenCoins.set(random.nextInt(4));
	}
	
	public int getBet() {
		return bet.get();
	}
	
	public IntegerProperty getBetProperty() {
		return bet;
	}
	
	public void setBet(int bet) {
		if (bet < 0)
			throw new IllegalArgumentException("L'aposta ha de ser positiva");
		this.bet.set(bet);
	}
	
	public void bid() {
		/*
		 * TODO: assigna a la aposta (bet) d'aquest jugador un nombre
		 * aleatori entre 0 i la quantitat màxima de monedes que hi
		 * poden haver segons el nombre de jugadors.
		 */
	}
}
