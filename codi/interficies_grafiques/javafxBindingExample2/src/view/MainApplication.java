package view;

import java.util.Random;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.Model;

public class MainApplication extends Application implements EventHandler<ActionEvent>{
	private Label valueLabel = new Label();
	private Button randomButton = new Button("Valor aleatori");
	private Button increaseButton = new Button("Augmenta valor");
	private Button decreaseButton = new Button("Disminueix valor");
	
	private Model model = new Model();
	
	private static final Random RANDOM = new Random();

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		HBox root = new HBox();
		root.setPadding(new Insets(10));
		root.setSpacing(10);
		
		root.getChildren().addAll(valueLabel, randomButton, increaseButton, decreaseButton);

		// Vinculem la propietat del model al text de l'etiqueta 
		valueLabel.textProperty().bind(model.getProperty().asString());
		// Afegim un listener a la propietat del model
		model.getProperty().addListener((observable, oldValue, newValue)->
				System.out.println("Nou valor: "+newValue));
		
		// Els botons permeten modificar el valor de la propietat
		randomButton.setOnAction(this);
		increaseButton.setOnAction(this);
		decreaseButton.setOnAction(this);
		
		Scene scene = new Scene(root, 500, 100);

		primaryStage.setTitle("Exemple binding");
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}

	@Override
	public void handle(ActionEvent event) {
		Object obj = event.getSource();
		
		if (obj == randomButton) {
			model.setValue(RANDOM.nextInt(100));
		} else if (obj == increaseButton) {
			model.setValue(model.getValue()+1);
		} else {
			model.setValue(model.getValue()-1);
		}
		
	}

}
