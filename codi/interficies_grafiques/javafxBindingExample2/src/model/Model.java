package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Model {
	private IntegerProperty property =
			new SimpleIntegerProperty(10);
	
	public int getValue() {
		return property.get();
	}
	
	public IntegerProperty getProperty() {
		return property;
	}
	
	public void setValue(int n) {
		property.set(n);
	}
}
