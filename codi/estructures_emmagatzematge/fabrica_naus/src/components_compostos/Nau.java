package components_compostos;

import components_abstractes.ComponentCompost;
import components_abstractes.ComponentNau;

public class Nau extends ComponentCompost {
	public Nau(String nom, int capacitat, int pes) {
		super(capacitat, pes);
		setNom(nom);
	}
	
	@Override
	public boolean add(ComponentNau component) {
		if (component instanceof Nau)
			throw new IllegalArgumentException("No es pot posar una nau dins d'una altra.");
		return super.add(component);
	}
	
	@Override
	public String toString() {
		return "Nau "+getNom()+"; capacitat: "+getCapacitat()+", pes: "+getPes();
	}
}
