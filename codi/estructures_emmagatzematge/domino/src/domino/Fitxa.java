package domino;

import java.util.ArrayList;
import java.util.List;

/**
 * La classe Fitxa representa una fitxa del domino. Una fitxa té dos
 * nombres del 0 al 6.
 */
public class Fitxa {
	/**
	 * La llista que guardarà els dos valors de la fitxa.
	 */
	private List<Integer> valors = new ArrayList<Integer>();
	
	/**
	 * El constructor rebrà dos nombres i inicialitzarà la fitxa.
	 * 
	 * @param a  un dels valors de la fitxa, de 0 a 6.
	 * @param b  l'altre valor de la fitxa, de 0 a 6.
	 * @throws IllegalArgumentException  si els valors no estan entre 0 i 6.
	 */
	public Fitxa(int a, int b) throws IllegalArgumentException {
		if (a<0 || a>6 || b<0 || b>6)
			throw new IllegalArgumentException("Els valors han d'estar entre 0 i 6");
		valors.add(a);
		valors.add(b);
	}
	
	/**
	 * Retornarà els punts que val la fitxa, és a dir, la suma dels seus valors.
	 * 
	 * @return  la suma dels valors de la fitxa.
	 */
	public int punts() {
		return get(Posicio.DRETA)+get(Posicio.ESQUERRA);
	}
	
	/**
	 * Comprova si la fitxa encaixa amb el nombre rebut o no.
	 * 
	 * @param n El nombre amb què es vol fer encaixar la fitxa.
	 * @return Una Posicio indicant si n és a la dreta de la fitxa,
	 * a l'esquerra, o no hi és a la fitxa.
	 */
	public Posicio encaixa(int n) {
		Posicio pos = Posicio.CAP;
		if (get(Posicio.DRETA) == n)
			pos = Posicio.DRETA;
		else if (get(Posicio.ESQUERRA) == n)
			pos = Posicio.ESQUERRA;
		return pos;
	}
	
	/**
	 * Retorna el nombre que hi ha en una de les posicions de la fitxa.
	 * 
	 * @param posicio  La posició de la fitxa que es vol obtenir (DRETA o ESQUERRA).
	 * @return el valor de la fitxa en aquesta posició (de 0 a 6).
	 * @throws IllegalArgumentException  si es consulta Posicio.CAP.
	 */
	public int get(Posicio posicio) throws IllegalArgumentException {
		int num;
		if (posicio == Posicio.CAP)
			throw new IllegalArgumentException("La posició ha de ser dreta o esquerra");
		if (posicio == Posicio.DRETA)
			num = valors.get(1);
		else
			num = valors.get(0);
		return num;
	}
	
	/**
	 * Capgirarà els valors. Per exemple, si són [2|4] es guardaran com a [4|2].
	 */
	public void gira() {
		List<Integer> nousValors = new ArrayList<Integer>();
		
		for (Integer valor : valors)
			nousValors.add(0, valor);
		
		valors = nousValors;
	}
	
	/**
	 *  Retornarà una cadena amb els valors de la fitxa en el format [a|b].
	 */
	@Override
	public String toString() {
		return "["+get(Posicio.ESQUERRA)+"|"+get(Posicio.DRETA)+"]";
	}
	
	/**
	 * Retorna si dues fitxes són iguals. Dues fitxes són iguals si i només si
	 * contenen els mateixos valors. L'ordre com es troben aquests valors no
	 * té rellevància. Per exemple, [2|3] és igual a [3|2].
	 * 
	 * @param Object o  l'objecte que es compararà amb aquesta fitxa.
	 * @return true si o és una Fitxa i té els mateixos valors que aquesta, false
	 * en cas contrari. 
	 */
	@Override
	public boolean equals(Object o) {
		boolean iguals = false;
		if (o instanceof Fitxa) {
			Fitxa f = (Fitxa) o;
			if ((valors.get(0) == f.valors.get(0) && valors.get(1) == f.valors.get(1)) ||
					(valors.get(0) == f.valors.get(1) && valors.get(1) == f.valors.get(0))) {
				iguals = true;
			}
		}
		return iguals;
	}
	
	/**
	 * Retorna un codi hash tal que el codi hash de dues fitxes iguals segons equals()
	 * és el mateix, i és diferent si les fitxes són diferents.
	 * 
	 * @return un codi hash.
	 */
	@Override
	public int hashCode() {
		int petit = Math.min(valors.get(0), valors.get(1));
		int gran = Math.max(valors.get(0), valors.get(1));
		return petit*6+gran;
	}
}
