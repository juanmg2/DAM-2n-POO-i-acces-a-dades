package cerca_binaria.d;

public class Comprova {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LlistaOrdenada l = new LlistaOrdenada();
		Integer[] nums = {9,0,8,1,7,2,6,3,5,4,1,5,7,3,9};
		for (int i=0; i<nums.length; i++) {
			l.add(nums[i]);
		}
		mostraLlista(l);
		System.out.println();
		for (int i=0; i<nums.length; i+=2) {
			l.remove(nums[i]);
		}
		mostraLlista(l);
	}
	
	public static void mostraLlista(LlistaOrdenada l) {
		for (int i=0; i<l.size(); i++) {
			System.out.println(l.get(i));
		}
	}

}
