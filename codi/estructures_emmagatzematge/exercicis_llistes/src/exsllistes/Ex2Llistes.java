package exsllistes;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Ex2Llistes {

	public static boolean comprovaOrdre(List<Integer> llista) {
		/*
		 * Implementació senzilla. Pot ser poc eficient si és una llista enllaçada
		 * amb molts elements
		 *
		for (int i = 0; i < llista.size() - 1; i++)
			if (llista.get(i) > llista.get(i + 1))
				return false;
		 */
		// Implementació amb iteradors
		boolean ordenada = true;
		Iterator<Integer> it = llista.iterator();
		int actual=0, anterior;
		
		if (it.hasNext())
			actual = it.next();
		
		while (it.hasNext() && ordenada) {
			anterior = actual;
			actual = it.next();
			if (anterior > actual)
				ordenada = false;
		}
		return ordenada;
	}

	public static void main(String[] args) {
		List<Integer> l1 = Arrays.asList(1, 3, 5, 7, 8, 10, 12);
		List<Integer> l2 = Arrays.asList(1, 3, 5, 10, 8, 12);

		System.out.println(comprovaOrdre(l1));
		System.out.println(comprovaOrdre(l2));
	}
}
