package exemple_encapsulacio;

public class ExempleEncapsulacio {
	public static void main(String[] args) {
		CircumferenciaV1 c = new CircumferenciaV1(5);

		System.out.format("Una circumferència de radi %.1f té una àrea de %.1f.", c.radi, c.area);
	}
}
