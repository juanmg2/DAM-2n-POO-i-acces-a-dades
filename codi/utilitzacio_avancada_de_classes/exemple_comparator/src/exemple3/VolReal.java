package exemple3;

import java.time.ZonedDateTime;
import java.util.Comparator;

public class VolReal {
	private ZonedDateTime sortida;
	private ZonedDateTime arribada;
	
	/**
	 * Aquest codi és funcionalment idèntic al de l'exemple 2, però utilitza
	 * les funcions lambda introduïdes a la versió 8 del Java.
	 * 
	 * El compilador sap que un Comparator és una intefície amb un sol mètode,
	 * així que implícitament ja sap quin mètode hem de sobreescriure.
	 * 
	 * Als parèntesis indiquim quin nom donem als paràmetres, i després de la
	 * fletxa el valor de retorn de la funció.
	 */
	public static final Comparator<VolReal> COMPARADOR_SORTIDES = (v1, v2) -> v1.getSortida().compareTo(v2.getSortida());
	public static final Comparator<VolReal> COMPARADOR_ARRIBADES = (v1, v2) -> v1.getArribada().compareTo(v2.getArribada());
	
	public VolReal(ZonedDateTime sortida, ZonedDateTime arribada) {
		this.sortida = sortida;
		this.arribada = arribada;
	}
	
	public ZonedDateTime getSortida() {
		return sortida;
	}
	
	public ZonedDateTime getArribada() {
		return arribada;
	}
}
