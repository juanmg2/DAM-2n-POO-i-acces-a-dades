package exercicis_operacions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Cp {

	public static void main(String[] args) {
		if (args.length == 2) {
			Path origen = Paths.get(args[0]);
			Path desti = Paths.get(args[1]);
			if (Files.isRegularFile(origen)) {
				// Si el destí és un directori, guardem amb el mateix
				// nom que el fitxer original.
				if (Files.isDirectory(desti))
					desti = desti.resolve(origen.getFileName());
				try {
					Files.copy(origen, desti);
				} catch (IOException e) {
					System.err.println("No s'ha pogut copiar: "+e.getMessage());
				}
			} else {
				System.err.println("Origen ha de ser un fitxer");
			}
		} else {
			System.err.println("Ús: java Cp origen destí");
		}

	}

}
