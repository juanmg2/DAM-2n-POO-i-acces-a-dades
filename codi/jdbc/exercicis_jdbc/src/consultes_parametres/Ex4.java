package consultes_parametres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Ex4 {
	/**
	 * 4- Fes un programa que permeti fer cerques de pel·lícules per paraula
	 * clau. El programa ha de cercar la paraula clau en el títol i en la
	 * descripció. Per cada pel·lícula trobada, s'ha d'indicar a quines botigues
	 * es pot trobar.
	 */
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		String user = "root";
		String passwd = "usbw";
		String sql = " SELECT DISTINCT title, store_id, address"
				+ " FROM film"
				+ " JOIN inventory USING(film_id)"
				+ " JOIN store USING(store_id)"
				+ " JOIN address USING(address_id)"
				+ " WHERE title LIKE(UCASE(?))"
				+ " OR description LIKE(UCASE(?))";
		Scanner entrada = new Scanner(System.in);
		try (Connection connexio = DriverManager.getConnection(url, user, passwd);
				PreparedStatement statement = connexio.prepareStatement(sql);) {
			String paraulaClau;
			do{
				System.out.println("Introdueix una paraula clau per cercar: ");
				paraulaClau = entrada.nextLine().trim();
				if (paraulaClau.length() > 0) {
					statement.setString(1, paraulaClau);
					statement.setString(2, paraulaClau);
					try (ResultSet rs = statement.executeQuery()) {
						if (rs.isBeforeFirst()) {// Significa que sí que hi ha registres
							String title;
							String titleAnterior = "";
							while (rs.next()) {
								title = rs.getString(1);
								System.out.print((!title.equals(titleAnterior)?"\nTítol: " + rs.getString(1):"") + 
										"\n ID botiga: "+ rs.getString(2) + "\n Adreça botiga: "+ rs.getString(3) + "\n");
								titleAnterior=title;
							}
						} else
							System.out.println("No s'ha trobat cap pel·lícula per la paraula clau "+ paraulaClau + ".\n");
					}
				} else {
					System.out.println("S'ha d'introduir almenys un caràcter.\n");
				}
			} while(paraulaClau.length()==0);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		entrada.close();
	}
}
