package consultes_parametres.ex5;

public class Film {
	public final String title;
	public final String description;
	public final String releaseYear;
	public final String actors;
	public final String language;
	
	public Film(String title, String description, String releaseYear, String actors, String language) {
		this.title = title;
		this.description = description;
		this.releaseYear = releaseYear;
		this.actors = actors;
		this.language = language;
	}
}
