## Utilització avançada de classes

### Exercicis

1. [Exercici del jardí](jardi.md)
2. [Exercici de l'Scrabble](scrabble.md)
3. [Exercici del zoo](zoo.md)
4. [Exercici dels fongs](fungus.md)
