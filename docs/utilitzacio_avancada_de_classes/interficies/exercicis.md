#### Exercicis

1. Creeu la classe *Persona* amb els atributs pes, edat i alçada. Implementeu
la interfície *Comparable* per fer que es compari l'alçada. Creeu un
*Comparator* per permetre que dues persones es puguin comparar per l'edat, i un
altre per tal que es puguin comparar pel pes.

2. La interfície *Serie* representarà una sèrie de nombres, de manera que, a
partir d'un nombre inicial es generarà la resta de la sèrie.
  * Crea aquesta interfície, amb els mètodes adequats per donar el nombre
  inicial, per inicialitzar-la a uns valors per defecte, i per obtenir els
  següents nombres de la sèrie.
  * Crea la classe *PerDos*, que generi la sèrie resultant de multiplicar per
  dos el nombre anterior.
  * Crea la classe *NegEntreDos*, que generi la sèrie resultant de canviar el
  signe i dividir entre dos el nombre anterior.
  * Fes un mètode que rebi una sèrie qualsevol i un nombre, i que mostri els
  primers 20 nombres de la sèrie.
  * Exemplifica l'ús d'aquest mètode amb un programa.

3. En un joc, dos jugadors enfrontaran tirades de daus segons les següents
regles:
  * Cada jugador tirarà un cert nombre de daus.
  * Cada jugador, a més, tindrà un valor a l'atac i un valor a la defensa, que
  seran nombres enters.
  * Després que els dos jugadors tirin els daus, el dau més gran d'una tirada
  s'emparellarà amb el dau més gran de l'altra, i així successivament. Si un
  jugador tira més daus que l'altre, els daus sobrants s'emparellen amb valors
  de 0.
  * Per cada parella de daus, si la suma de l'atac d'un jugador amb el valor del
  seu dau és més gran que la suma de la defensa de l'altre més el seu dau, es
  suma un punt a aquest jugador.
  * El resultat final de l'enfrontament és la diferència entre els punts
  obtinguts pels dos jugadors.  
  ***Exemple:***
  ```
  Jugador 1: Atac: 3, defensa 1, daus 5, tirada:  1,  1, 4, 4, 5
  Jugador 2: Atac: 2, defensa 2, daus 4, tirada: (0), 2, 4, 5, 5
  Punts pel primer jugador: primer dau (1+3>0+2), tercer dau (4+3>4+2) i cinquè dau (5+3>5+2). Total: 3 punts.
  Punts pel segon jugador: segon dau (2+2>1+1), tercer dau (4+2>4+1), quart dau (5+2>4+1) i cinquè dau (5+2>5+1). Total: 4 punts.
  Resultat de l'enfrontament: 3 – 4 = -1.
  ```
  * Crea la classe Tirada amb els atributs d'atac, defensa i una tirada de
  daus.
  * Crea un constructor per *Tirada* que rebi els valors de l'atac i la
  defensa i el nombre de daus a tirar, i que creï la tirada de daus
  corresponent (és a dir, tiri els daus).
  * Sobreescriu el mètode *toString*. Fes que retorni una cadena que mostri
  la tirada. Per exemple, per la primera tirada de l'exemple, retornaria:
  ```
  AT: 3  DEF: 1  Daus (5): 11445
  ```
  * Fes que la classe *Tirada* implementi la interfície *Comparable*. El
  mètode *compareTo* retornarà el resultat de l'enfrontament entre dues
  tirades.
  * Fes que la classe *Tirada* implementi la interfície *Cloneable*.
  * Crea la classe *ProvaTirada* amb un mètode *main* que demostri l'ús de
  la classe Tirada. Aquest mètode escriurà per pantalla alguna cosa
  similar a:
  ```
  AT: 3 DEF: 1 Daus (5): 11445
  AT: 2 DEF: 2 Daus (4): 2455
  Comparació de les dues tirades: -1
  Comparació d'una tirada amb una d'igual: 0
  ```
