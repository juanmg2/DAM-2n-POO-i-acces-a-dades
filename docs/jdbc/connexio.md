## Establiment de connexions

### Incorporació del driver al projecte

Per poder establir una connexió a MySQL/MariaDB des de Java, primer de tot
cal incorporar el *driver* al nostre projecte d'Eclipse.

Això es pot automatitzar utilitzant un sistema com *Maven* o *Gradle* (ho
farem així en altres temes), o també podem importar directament el fitxer
*jar* del driver al projecte.

Els connectors per MySQL i MariaDB són, en principi, intercanviables, però
és més segur si utilitzem el connector adequat al sistema que estiguem
utilitzant.

El connector per MariaDB es pot descarregar de
https://code.mariadb.com/connectors/java/. El de MySQL de
http://dev.mysql.com/downloads/connector/j/.

En qualsevol dels dos casos hem d'acabar amb un fitxer *jar*.

Copiem el *driver* a la carpeta del projecte, seleccionem el projecte a
l'Eclipse i premem *F5* per tal que actualitzi el projecte segons el sistema
de fitxers.

Anem a les propietats del projecte, i seleccionem *Java Build path* i
*Libraries*. Anem a *Add JARs...* i selecccionem el fitxer *jar*.

### Prova de connexió

Intentarem ara establir una connexió amb el servidor.

Les classes que apareixen als següents exemples s'han d'importar dels paquets
*java.sql* i *javax.sql*. En aquests paquets es defineixen les interfícies
que han de complir els *drivers* i cada *driver* proporciona les seves
implementacions pròpies.

El següent exemple estableix una connexió contra el servidor local:

```java
public class Connectar {
	public static void main(String[] args) {
		try (Connection con =
				DriverManager.getConnection("jdbc:mariadb://localhost:3306/sakila?user=root&password=usbw")) {
			System.out.println("Connexió exitosa a la base de dades!");
		} catch (SQLException e) {
			System.err.println("Error d'establiment de connexió: " + e.getMessage());
		}
	}
}
```

Mereix especial atenció la línia de connexió:

```
jdbc:mysql://localhost:3306/demo?user=root&password=usbw
```

- En primer lloc apareix el protocol. Aquí hem de posar `jdbc:mariadb` si
ens connectem a un servidor MariaDB, o `jdbc:mysql` si ens connectem a MySQL.

- Després tenim l'adreça on ens connectem, en el nostre cas es tracta del
mateix ordinador (*localhost*), al port 3306, que és el port per defecte
de MySQL/MariaDB.

- A continuació tenim la base de dades que volem utilitzar, a l'exemple
*sakila*.

- Després passem l'usuari i la contrasenya que utilitzem per establir la
connexió.

Noteu que el format de la línia de connexió és el mateix que el que s'utilitza
per indicar una URL al navegador web.


### Formes alternatives d'establir la connexió

El mètode *DriverManager.getConnection()* està sobrecarregat i admet diversos
formats per indicar els paràmetres de connexió.

En aquest exemple passem directament els paràmetres:

```java
public class Connectar2 {
	public static void main(String[] args) {
		String url = "jdbc:mariadb://localhost:3306/sakila";
		String user = "root";
		String passwd = "usbw";
		try (Connection con = DriverManager.getConnection(url, user, passwd)){
			System.out.println("Connexió exitosa a la base de dades!");
		} catch (SQLException e) {
			System.err.println("Error d'establiment de connexió: " + e.getMessage());
		}
	}
}
```

I en aquest últim exemple passem els paràmetres a través d'un object
*Properties*. La classe *Properties* és un *Map* que s'utilitza per guardar
parelles de *String*. S'utilitza sobretot per emmagatzemar les opcions
de configuració d'un programa a un fitxer.

```java
public class Connectar3 {
	public static void main(String[] args) {
		String url = "jdbc:mariadb://localhost:3306/sakila";
		Properties propietatsConnexio = new Properties();
		propietatsConnexio.setProperty("user", "root");
		propietatsConnexio.setProperty("password", "usbw");
		try (Connection con = DriverManager.getConnection(url, propietatsConnexio)){
			System.out.println("Connexió exitosa a la base de dades!");
		} catch (SQLException e) {
			System.err.println("Error d'establiment de connexió: " + e.getMessage());
		}
	}
}
```
