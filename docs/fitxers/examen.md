## Examen de fitxers

### Exercici 1. Traductor de binari a text.

Tenim un fitxer binari amb una colla de nombres enters guardats. Com que els
fitxers binaris són complicats de llegir per nosaltres, volem traduir-lo a un
fitxer de text que contingui exactament els mateixos nombres, separats cadascun
d'ells per un espai.

Fes un programa que faci això i que rebi per paràmetre (en els arguments del
*main*) dos noms de fitxer. El primer serà el nom del fitxer binari original,
i el segon el nom del fitxer de text resultant.

Comprova que el primer fitxer existeix i que el segon no existeix (no volem
esborrar un fitxer per accident).

Cal fer el programa sense llegir el primer fitxer sencer a memòria, sinó que
mantindrem els dos fitxers oberts i anirem llegint i guardant els nombres
un a un.

Per al fitxer d'exemple proporcionat, el fitxer resultant és:

```
5 0 3 -1 1234567 876535 -876443 1
```

### Exercici 2. *grep* amb context.

Volem fer una versió modificada de l'exercici del *grep simple* en què es
mostri, a més de la línia que conté la cadena de caràcters cercada, les dues
línies anteriors i les dues línies posteriors.

Per fer-ho, però, no volem guardar tot el fitxer en memòria, perquè podria ser
molt estens. En comptes d'això només guardarem, com a molt, les dues línies
immediatament anteriors a la línia que estiguem comprovant en un moment donat.

En cas que dues ocurrències de la cadena cercada es trobin molt juntes, cal
unir els diversos contextos en un de sol, com es mostra a l'exemple.

La sortida mostrarà cadascun dels contextos trobats, separats per línies de
guions.

Podeu partir de la solució de l'exercici del *grep simple* i modificar el mètode
*grep*.

Per al fitxer d'exemple proporcionat i la cadena de cerca *zz*, el resultat del
programa és:

```
línia 1 zz
línia 2
línia 3
línia 4
línia 5 zz
línia 6
línia 7
línia 8
línia 9
línia 10 zz
línia 11
línia 12 zz
línia 13 zz
línia 14
línia 15
--
línia 17
línia 18
línia 19 zz
línia 20
línia 21
--
línia 24
línia 25
línia 26 zz
línia 27
línia 28
--
línia 32
línia 33
línia 34 zz

```

### Exercici 3. Població per comarques

A partir del fitxer *poblacions.xml* (extret de
http://api.idescat.cat/pob/v1/geo.xml i simplificat), que conté les diverses
províncies i comarques de Catalunya conjuntament amb la seva població, volem
obtenir una llista de cadascuna de les regions i la seva població total.

El format de l'XML compleix que:

- El nom de les regions apareix a l'etiqueta *title*.
- El nom de les poblacions a mostrar per a cada població apareix a l'etiqueta
*Obs*.
- Dins de l'etiqueta *Obs* tenim un atribut que indica el sexe (*SEX*), volem
només la població total (*T*). La població és a l'atribut *OBS_VALUE*.

Heu de partir de l'exemple *LectorSAX*. Modifiqueu-lo per tal que:

- Al controlador tindrem un booleà per saber si s'ha obert un element *title*.

- A *startElement* posarem aquest booleà a true si s'obre un *title*.

- A *endElement* posarem aquest booleà a false si es tanca un *title*.

- A *characters* llegirem i mostrarem el nom de la població, només en cas que
estiguem dins d'un element *title*.

- A *startElement* comprovarem si l'element que s'obre un *Obs*. Si ho és,
comprovarem si l'atribut *SEX* és *T*. I si ho és, mostrarem per pantalla
el contingut de l'atribut *OBS_VALUE*.

El resultat d'executar el programa és:

```
Catalunya: 7508106
Barcelona: 5523922
Girona: 753054
...
Segrià: 209324
Selva: 168555
Solsonès: 13414
Tarragonès: 249939
Terra Alta: 11872
```
