##### Ús bàsic dels genèrics

El problema introduït a l'apartat anterior va motivar que en la versió 5
de Java s'introduís la noció dels genèrics.

Una possible solució al problema del tipus seria crear classes llista
diferents per a cada tipus de dada. Per exemple, si volem una llista
d'enters, programaríem la classe *IntegerList*, que només podria rebre
elements de tipus *Integer* i retornaria elements de tipus *Integer*, i
després faríem la classe *StringList*, per treballar amb llistes de
*String*. Això resoldria el nostre problema, però caldria reimplementar
la llista per a cada tipus de dada que volguéssim guardar. Acabaríem
tenint un munt de classes diferents que fan pràcticament el mateix, però
canviant el tipus de dades amb què treballen.

No seria genial poder passar aquest tipus de dada com a paràmetre quan
creem l'objecte llista, i que la mateixa implementació ens servís per a
crear llistes per a qualsevol d'ells? Doncs aquesta és justament la idea
dels genèrics.

Naturalment, passar un tipus com a paràmetre no és exactament el mateix
que passar un valor, i la sintaxi canvia una mica. A partir d'ara, en
totes les referències que utilitzem a tipus de llista, i en tots els
*new*, especificarem entre \< \> el tipus de dada adequat:

```java
public class ProvaLlistes {
   public static void main(String[] args) {
       // Creem una llista buida
       List<String> llista = new ArrayList<String>();
       // Creem una cadena d'entrada
       String sin = "prova llistes";
       // Afegim la cadena a la llista
       llista.add(sin);
       // Traiem la cadena de la llista
       String sout = llista.get(0);
   }
}
```

En aquest exemple s'ha eliminat la necessitat del cast, i ara sentències
com:

```java
Integer iin = 3;
llista.add(iin); // ERROR
```

o

```java
Integer iout = llista.get(0); // ERROR
```

donen error de compilació. Si intentem introduir o extreure una
referència de la llista de tipus invàlid, ho notarem al mateix moment
d'escriure el codi, i podrem arreglar l'error immediatament. Ja no
caldrà comprovar els tipus ni gestionar aquestes excepcions.

Tornem a mirar ara l'ajuda de la interfície *List*. Quan veiem coses
com: `public interface List<E>`, aquesta *E* es refereix al
tipus de dada que hem de passar com a paràmetre quan utilitzem la
interfície *List*. Per exemple, el mètode `boolean add(E e)`,
ens indica que hem de passar un objecte del mateix tipus pel qual hem
declarat la llista, i `E get(int index)` ens indica que aquest
mètode retorna una referència d'aquest tipus.

La implementació de classes pròpies que utilitzin genèrics presenta
algunes dificultats relacionades amb el polimorfisme (el fet que un
objecte pugui ésser de diversos tipus a la vegada), i queda fora de
l'àmbit del curs, però en podeu llegir una bona introducció al tutorial
oficial sobre genèrics a <https://docs.oracle.com/javase/tutorial/java/generics/>.
