#### Exercicis de llistes

##### Exercici 1

Demana cadenes a l'usuari fins que una d'elles sigui buida. Guarda totes
les cadenes que ha introduït en una llista. Després, torna a demanar
cadenes i per a cadascuna, fes que el programa digui si la cadena s'ha
introduït abans o no.

##### Exercici 2

Fes una funció que rebi com a paràmetre una llista de nombres i que
retorni *true* si la llista està ordenada de menor a major i *false* en
cas contrari.

*Nota*: No s'ha d'ordenar la llista, ni copiar-la, ni modificar-la, només
veure si està ordenada o no.

##### Exercici 3

Donada la següent llista de popularitat dels llenguatges, on el primer
és el més popular i l'últim el que menys:

`llenguatges = "Java", "C", "C++", "Visual Basic", "PHP", "Python", "Perl", "C#", "JavaScript","Delphi"`

Extreu (no cal que facis un menú, simplement que el programa extregui
seqüencialment):

* El llenguatge més popular de la llista.
* El sisè més popular.
* Els tres més populars.
* Els tres menys populars.
* Tots menys el primer.
* Imprimeix la llista amb cada llenguatge en una línia i numerada.
* Nombre total de llenguatges a la llista.

*Nota*: utilitza un iterador sempre que hagis de recórrer una part important
de la llista.

##### Exercici 4

Donada la llista: `llista1=[10,20,30,40,50,60,70,80,90]`
realitzar les següents operacions utilitzant els mètodes de les llistes:

* Imprimir el primer i l'últim elements.

* Imprimir la quantitat de nombres que té la llista.

* Comprovar que existeix l'element 70 i, si és així, esborrar-lo.

* Esborrar l'element que es troba en la primera posició (la llista queda:
`[20,30,40,50,60,80,90]`).

* Intercanviar l'element 20 pel 30.

* Afegir el nombre 100 al final de la llista.

* Afegir el nombre 35 entre el 20 i el 40 (pot especificar-se la posició
  directament; la llista queda: `[30,20,35,40,50,60,80,90,100]`).

* Preguntar a l'usuari un nombre i dir-li si el nombre és a la llista o no.

* Substituir el 20 i el 100 de la llista pel nombre 50 (la llista queda:
 `[30,50,35,40,50,60,80,90,50]`).

* Imprimir la quantitat de vegades que apareix el 50.

* Imprimir tots els elements excepte el primer i l'últim.

* Imprimir els que estiguin en posició parella (50,40,60,90).

* Afegir a la llista (en un sol pas) el contingut d'aquesta altra
llista:`llista2=[200,210,220,230]` (la llista queda:
`[30,50,35,40,50,60,80,90,50,200,210,220,230]`).

* Invertir la llista (la llista queda:
  `[230,220,210,200,50,90,80,60,50,40,35,50,30]`).

* Eliminar (sense especificar la seva posició i sense saber a priori quants
n'hi ha) tots els nombres 50 de la llista. Per evitar que salti una
excepció, comprovar primer si hi ha elements 50 a la llista.

* Copiar la llista a una altra llista independent (*llista2*) i invertir-la
(*llista1* queda: `[230,220,210,200,90,80,60,40,35,30]` i *llista2* queda:
`[30,35,40,60,80,90,200,210,220,230]`).

* Crear una tercera llista sumant els elements de *llista1* i *llista2*
per posicions (només sabem que les llistes són iguals, però no sabem
la seva longitud; *llista2* queda:
`[260,255,250,260,170,170,260,250,255,260]`).

* Imprimeix la relació de nombres i el número de vegades que apareix a la
llista. Exemple:

    ```
260 → 3
255 → 2
250 → 2
```

##### Exercici 5

En aquest exercici implementarem una petita agenda. Crea la classe
*Contacte* per a guardar la informació de cadascun dels contactes, i la
classe *Agenda*, que contindrà una llista de contactes.

Fes un programa que a través d'un menú permeti realitzar les operacions
habituals sobre una agenda:

* Afegir elements (s'inseriran mantenint l'ordre alfabètic per cognom i nom).
* Esborrar algun element, donat el seu nom.
* Modificar el nom, cognom, adreça o telèfon d'un element.
* Cercar un element donada alguna de les dades.

##### Exercici 6

Per a representar un enter llarg, de més de 30 xifres, utilitzarem un
vector dinàmic (*ArrayList*), que contingui en el camp dades de cada
node una xifra de l’enter.

Escriviu una classe *EnterLlarg* que encapsuli l'*ArrayList* i que
permeti inicialitzar-lo amb un long, un vector de byte, o un *String*
amb la representació del nombre.

Escriviu un programa en què es llegeixin dos enters llargs de teclat, es
creïn els corresponents *EntersLlargs* i es mostrin per pantalla
(recorrent la llista per obtenir de nou les xifres). Per això caldrà
sobreescriure el mètode *toString* de la classe *EnterLlarg*.

Obteniu i mostreu una nova llista que emmagatzemi la suma dels dos
nombres anteriors. Per fer això caldrà afegir el mètode suma a la classe
*EnterLlarg*.

Compara aquesta implementació amb la classe *BigInteger* de Java. Creus
que internament s'han implementat de forma similar?

##### Exercici 7

La **cerca binària** o **cerca dicotòmica** és un algorisme de cerca. Per a
realitzar-la, és necessari comptar amb un vector o llista ordenada.
Llavors, prenem l'element que es troba a la mitat de la llista, i el
comparem amb l'element cercat. Si l'element cercat és menor, es pren
l'interval que va des de l'element central al principi. En cas contrari,
prenem l'interval que va des de l'element central fins al final de la
llista. Procedim així, amb intervals cada vegada menors, fins que
arribarem a un interval indivisible. Llavors, o l'element no és a la
llista, o l'interval trobat és el nostre element.

Amb aquests exercicis comprovarem el funcionament de la cerca binària i
practicarem la implementació d'estructures dinàmiques de dades.

* Crea la classe *LlistaOrdenada* que contingui una *List* i que permeti
introduir i suprimir *Integer* a la llista, de manera que els enters sempre
es guardaran de forma ordenada. Discuteix els avantatges i inconvenients
d'utilitzar les diverses implementacions de *List* disponibles. Utilitza
un *ArrayList*.

* Implementa l'algorisme de cerca binària al mètode *cerca* de la classe
*LlistaOrdenada*. El mètode ha de retornar la posició que ocupa l'element
cercat dins de la llista, o -1 si l'element no hi és.

* (*Opcional*) Compara les velocitats del mètode de cerca binària amb
la cerca normal que proporciona el mètode *indexOf* de la interfície *List*.
Quin mètode és millor? Per què creus que passa això? Pots quantificar quant
més ràpid és un sistema respecte l'altre? Utilitza el mètode
*System.currentTimeMillis()* per obtenir el temps en un moment donat. Crea
una llista amb molts elements i crida els mètodes de cerca moltes vegades
per evitar que els temps de resolució siguin massa petits per poder-los
comparar.

* Modifica la classe *LlistaOrdenada* per fer que l'addició i supressió
d'elements utilitzin el mètode de cerca binària per trobar a quina posició
s'ha d'afegir o treure un element.

* (*Opcional*) Modifica la classe *LlistaOrdenada* per fer que permeti
guardar qualsevol tipus d'objecte que implementi la interfície
*Comparable*. Fes que la cerca binària utilitzi l'ordenació pròpia de les
classes que es guarden a la llista.

* (*Opcional*) Modifica la classe *LlistaOrdenada* per tal que utilitzi
tipus genèrics. És a dir, fes que la teva classe permeti crear objectes
com per exemple:

    `LlistaOrdenada<Integer> l = new LlistaOrdenada<Integer>();`

* Investiga si existeix alguna implementació de les llistes ordenades i
de l'algorisme de cerca binària que ja estigui inclòs a les API de Java.
Què has trobat? Modifica la classe *LlistaOrdenada* per fer que utilitzi la
cerca binària proporcionada per les API en comptes de la teva implementació.

* El mètode *sort*, per ordenar una llista, no està inclòs a la
interfície *List* ni a les classes *LinkedList* o *ArrayList*, sinó que
s'ha implementat com a mètode estàtic de la classe *Collections*. Per
què creus que s'ha dissenyat d'aquesta manera?

* Creus que hi pot haver algun problema amb la cerca binària si es
modifiquen els valors dels atributs dels objectes que hi ha guardats
a la llista? Llegeix la introducció de la interfície *Set* a la
documentació de les API i fixa't en la nota que hi ha sobre elements
mutables. Per què creus que hi ha aquesta advertència?

* (Opcional) Vés a la pàgina
http://www.oopweb.com/Java/Documents/IntroToProgrammingUsingJava/Volume/c12/exercises.html
i fes l'exercici 12.2 sobre la interfície *Set*. Allà mateix hi ha la solució.

##### Exercici 8

Crea la classe *Punt*, amb dues variables membre de tipus *double*, que
representaran les coordenades *x* i *y* del punt.

Crea la classe *Cami* que contindrà una llista de punts. Proveeix els
mètodes necessaris per poder afegir punts al camí. A més:

* Implementa el mètode *mida*, que retornarà la quantitat de punts del camí.

* Implementa el mètode *toString*, que retornarà una cadena mostrant els
punts del camí.

* Implementa el mètode *distancia*, que retornarà la distància recorreguda
pel camí, és a dir, la suma de les distàncies entre cada parella de punts consecutius.

Fes un programa que demostri l'ús de la classe *Cami*.

##### Exercici 9

Per implementar un joc de cartes necessitarem la classe *Carta* i la
classe *Baralla*.

###### Classe *Carta*

Cada objecte de la classe *Carta* representarà una carta de la baralla
espanyola.

* La classe *Carta* tindrà 4 constants per facilitar la lectura dels pals
de les cartes. *OROS* valdrà 0, *COPES* valdrà 1, *ESPASES* valdrà 2,
i *BASTOS* valdrà 3.

* El constructor de *Carta* rebrà un nombre de l'1 al 12, que serà el
número de la carta, i el seu pal: una constant de les anteriors (és
a dir, un nombre de 0 a 3). Crea mètodes *get* adequats per obtenir el pal
i el nombre de la carta.

* Sobreescriu el mètode *toString* per tal de tornar una representació
de la carta. Per exemple, el 8 de bastos retornaria la cadena “8B”.

###### Classe *Baralla*

La classe *Baralla* tindrà un atribut de tipus llista on
s'emmagatzemaran totes les cartes que hi hagi a la baralla.

* El constructor de *Baralla* crearà les 48 cartes de la baralla espanyola
i les posarà a la llista, ordenades (primer els oros, de l'1 fins al 12,
després les copes, després les espases i finalment els bastos).

* El mètode *remena* barrejarà les cartes. Per fer-ho, es crearà una
llista nova. Després es recorreran totes les cartes de la baralla i, per
cadascuna, es triarà una posició aleatòria a la nova llista, on
s'inserirà. Quan s'hagin inserit totes les cartes, es prendrà la nova
llista com a la pròpia de la baralla.

Per exemplificar, es mostren els primers passos:

* Primera carta, 1O. La llista queda: 1O.
* Segona carta 2O. Nombre aleatori entre 0 i 1. Si surt 0, queda
2O 1O, si surt 1, queda 1O 2O. Suposem el primer cas.
* Tercera carta 3O. Nombre aleatori entre 0 i 2. Les possibilitats
són: 3O 2O 1O, 2O 3O 1O, o 2O 1O 3O.
* El mètode *roba* retornarà la primera carta de la baralla i l'eliminarà
de la llista.
* El mètode *eliminaNum* rep un nombre i elimina de la baralla totes les
cartes que tinguin aquest nombre. Cal utilitzar un iterador.
* El mètode *toString* retornarà una cadena que mostri totes les cartes
que hi ha a la baralla.
* Crea un mètode *main* en una altra classe que exemplifiqui l'ús d'una
baralla.

##### Exercici 10

Per les següents operacions sobre una llista, marca en quina de les dues
implementacions es farà més ràpidament, o si el temps serà similar:

|Operació|Llista enllaçada|Vector dinàmic|
|------|--------------|------------|
|Inserir un element a continuació d'un node determinat.|||
|Accedir al node que ocupa la posició 10 a la llista.|||
|Accedir al primer o últim node de la llista.|||
|Afegir 5 elements al final de la llista.|||
|Afegir 5 elements al principi de la llista.|||
|Esborrar el node que ocupa la tercera posició a la llista.|||
|Recórrer tots els nodes d'una llista, sumant 1 al seu contingut.|||
|Trobar un cert element, si la llista no està ordenada.|||
|Trobar un cert element, si la llista està ordenada.|||
|Crear una nova llista que contingui una còpia dels nodes del 5 al 8 de la llista original.||||
